
掌上运维 zabbix 运维工具 2014-11-14 15:41:59

http://blog.chinaunix.net/uid-12765590-id-4622226.html

最近写了一套结合zabbix的掌上运维系统，可以实现ping、zabbix监控项图形化查看、服务器指定命令（如关机、重启进程等）、IPMI名（远程管理卡关机、重启、BMC重启等）、VM-esxi(esxi 虚拟化管理重启虚拟机、关机等）。
主要由CI（php MVC框架）、jqmobile(js的移动框架）写成。APP化也已经完成。

# 安装方法
先安装好zabbix，这个网上教程很多，我就不阐述了。

# 下面是部署掌上运维的的方法：
```
下载代码：
https://github.com/dongfangyiye/zsyw
下载完整的代码复制至zabbix的网页的根目录，记得包含zsyw目录。
修改配置：
修改zsyw/application/config/config.php
找到
$config['base_url']     = 'http://IP/zsyw/'
修改为
$config['base_url']     = 'http://你自己服务器的IP或者域名/zsyw/'
修改数据库密码：
修改zsyw/application/config/database.php
------------------------------------------------
$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '你zabbix的数据库密码';
$db['default']['database'] = 'zabbix';
$db['default']['dbdriver'] = 'mysql';
在zabbix数据库里添加数据库表：
CREATE TABLE `cm_history` (
  `history_id` int(11) NOT NULL auto_increment,
  `name` varchar(50) default NULL,
  `sipp` varchar(50) default NULL,
  `runcommand` varchar(50) default NULL,
  `output` varchar(50) default NULL,
  `status` varchar(50) default NULL,
  `ipaddress` varchar(50) default NULL,
  `sname` varchar(50) default NULL,
  `stamp` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`history_id`)
) ENGINE=MyISAM AUTO_INCREMENT=676 DEFAULT CHARSET=utf8;
```
好了，大功告成，现在你就能实现2个功能ping和监控项目图形（目前写死了项目后期考虑配置形式），服务器命令、IPMI、VMESXI需要另外配置.

# 安卓手机APP的代码:
https://github.com/dongfangyiye/webview_zsyw

还有就是zabbix 默认的admin是没有机器组关联的，所以你需要新建一个账号和主机组关联。有问题可以和我交流。

登陆
